import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {User} from "@pdb/login/model/user";
import {throwError} from "rxjs";
import {UserPrincipalModel} from "@pdb/shared/models/user-principial.model";
import {catchError, tap} from "rxjs/operators";
import {StorageService} from "@pdb/shared/storage/storage.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient,
              private storageService: StorageService,
              private router: Router) {
  }

  login(user: User): void {
    this.http.post<any>('api/auth/login', user, {observe: "response"})
      .pipe(
        tap((response: any) => {
          const token = response.headers.get('authorization');
          const userPrincipal: UserPrincipalModel = <any>response.body;

          this.storageService.storeToken(token);
          this.storageService.storeUserPrincipal(userPrincipal);
        }),
        catchError(error => {
          return throwError(error)
        })
      )
      .subscribe(() => {
        this.router.navigate(['dashboard'])
      })
  }

  createAccount(user: User): void {
    this.http.post<any>('api/user/add', user, {observe: "response"})
      .pipe(
        tap((response: any) => {
          console.log('Konto zostało utworzone')
        }),
        catchError(error => {
          return throwError(error)
        })
      )
      .subscribe()
  }
}
