export interface UserPrincipalModel {
  username: string,
  enabled: boolean,
  credentialsNonExpired: boolean,
  accountNonLocked: boolean,
  accountNonExpired: boolean,
}
