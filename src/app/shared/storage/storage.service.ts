import {UserPrincipalModel} from "@pdb/shared/models/user-principial.model";
import {Injectable} from "@angular/core";

enum StorageKey {
  TOKEN = 'JWT',
  USER_PRINCIPAL = 'USER_PRINCIPAL',
}

@Injectable({
  providedIn: 'root'
})
export class StorageService {


  private localStorage : Storage;

  constructor() {
    this.localStorage = localStorage;
  }

  storeToken(token: string): void {
    this.setItem(StorageKey.TOKEN, token);
  }

  getToken(): string {
    return this.getItem(StorageKey.TOKEN);
  }

  storeUserPrincipal(userPrincipal: UserPrincipalModel) {
    this.setItem(StorageKey.USER_PRINCIPAL, userPrincipal);
  }

  getUserPrincipal(): UserPrincipalModel {
    return this.getItem(StorageKey.USER_PRINCIPAL);
  }

  clearAll(): void {
    this.localStorage.clear();
  }

  private setItem<T>(key: StorageKey, item : T) {
    this.localStorage.setItem(key, JSON.stringify(item));
  }

  private getItem<T>(key: StorageKey) : T {
    const objJson = this.localStorage.getItem(key);
    return objJson === null ? null : JSON.parse(objJson);
  }

}
